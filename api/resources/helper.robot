*** Keywords ***

Convert Dic to Json
    [Arguments]     ${dic}
    log  ${dic}
    ${json}=    evaluate    json.dumps(${dic})    json
    log  ${json}
    ${json_json}  To Json  ${json}  pretty_print=True
    log  ${json_json}
    [Return]        ${json_json}
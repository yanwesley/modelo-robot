*** Settings ***
Resource	${EXECDIR}/api/resources/main.resource
Suite Setup   Abrir conexao  http://191.234.181.234:3000

*** Test Cases ***

Cenário 01: CRUD na tela de Cadastro/Fabricantes
    POST
    GET
    PUT
    DELETE

*** Keywords ***

GET
    ${HEADERS}  Create Dictionary     
    ...  Content-Type=application/json
    ${RESPOSTA}     GET On Session      newsession    url=/api/v1/producers?originId=2&page=1&size=10
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    log   ${RESPOSTA.json()}
    ${GetIdFabricante}    Get Variable Value    ${RESPOSTA.json()['content'][0]['id']}
    Set Test Variable   ${GetIdFabricante}
    log  ${GetIdFabricante}

POST
    ${HEADERS}  Create Dictionary     
    ...  Content-Type=application/json
    ${json}  Catenate   
    ...  {
	...  "originId": 2,
	...  "description": "Creme",
	...  "user": "Yan"
    ...  }
    ${RESPOSTA}     POST On Session      newsession     /api/v1/producers
    ...     headers=${HEADERS}
    ...     data=${json}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    201
    log   ${RESPOSTA.json()}

PUT
    ${HEADERS}  Create Dictionary     
    ...  Content-Type=application/json
    ${json}  Catenate   
    ...   {
    ...  "description": "Creme Alterado",
    ...  "user": "Yan"
    ...  }
    ${RESPOSTA}     PUT On Session      newsession     /api/v1/producers/${GetIdFabricante}
    ...     headers=${HEADERS}
    ...     data=${json}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    log   ${RESPOSTA.json()}

DELETE
    ${HEADERS}  Create Dictionary     
    ...  Content-Type=application/json
    ${RESPOSTA}     DELETE On Session      newsession    /api/v1/producers/${GetIdFabricante}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    [Return]  ${RESPOSTA.json()}
    log   ${RESPOSTA.json()}
*** Settings ***
Resource   ${EXECDIR}/mobile/resources/main.resource

Documentation
...    Como usuário 
...    Gostaria de realizar um download
...    depois de 10 min eu abro o app 
...    e verifico se o conteudo está disponível
...    verifico se a midia é exibida 
...    e por último deleto a midia do dispositivo 

*** Test Cases ***

Cenário 01: Download de conteúdo
    [Tags] download
    Quando abro a aplicação
    clico em Explore
    em categorias clico netshowme
    clico na opção de download
    valido que o download está concluido
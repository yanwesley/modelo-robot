# README
## Projeto de Automação de Testes 
### Objetivo do projeto
Desenvolver scripts de teste utilizando Robot Framework

## Setup do projeto
### Pré requisitos:
[Python 3](https://www.python.org/downloads/release/python-375/) (O projeto foi desenvolvido utilizando a versão 3.9.1)  
[Git](https://git-scm.com/download/win)

## Instalando dependências
Dentro do projeto existe um arquivo `requirements.txt` que contém todas as dependências do projeto.
Instale-as em seu ambiente usando o comando:
```batchfile
> pip install -r requirements.txt
```

## Executando o projeto
Sempre através de linha de comando, basta informar o path do teste.
```batchfile
> robot -d ./Results web/cases/test.robot
```
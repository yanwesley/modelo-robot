*** Settings ***
Library             SeleniumLibrary
Library             RequestsLibrary
Library             Collections
Library             FakerLibrary  locale=pt_BRResource            main.resource
Resource            main.resource
Library             ImapLibrary2
Library             ExcelLibrary

*** Keywords ***
Abrir Navegador
    [Arguments]  ${url}
    ${list} =     Create List    --start-maximized    --disable-web-security  --headless  --no-sandbox  --disable-dev-shm-usage
    ${args} =     Create Dictionary    args=${list}
    ${desired caps} =     Create Dictionary  chromeOptions=${args}
    Open Browser                ${url}  browser=chrome  alias=None  remote_url=False  desired_capabilities=${desired_caps}  ff_profile_dir=None
    Maximize Browser Window
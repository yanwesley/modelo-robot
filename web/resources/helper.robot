*** Keywords ***

Nova Aba
    [arguments]     ${url}
    Execute Javascript          window.open()    
    Switch Window               locator=NEW
    Go To                       ${url}

Convert Dic to Json
    [Arguments]     ${dic}
    log  ${dic}
    ${json}=    evaluate    json.dumps(${dic})    json
    log  ${json}
    ${json_json}  To Json  ${json}  pretty_print=True
    log  ${json_json}
    [Return]        ${json_json}

Email Verification
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=imap.gmail.com    user=inserir o email aqui  password=inserir a senha aqui  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=12000
    ${body}    Get Email Body  ${LATEST}
    Should Contain    ${body}    ${content}
    Close Mailbox
    [Return]  ${body}

Discard Email
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=imap.gmail.com    user=inserir o email aqui  password=inserir a senha aqui  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=12000
    Delete Email  ${LATEST}